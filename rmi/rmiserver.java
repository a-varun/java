import java.rmi.server.*;
import java.rmi.*;
import s.*;

public class rmiserver{
	public static void main(String[] args) throws Exception{
		Remote remobj = new s.suminterfimpl();
		UnicastRemoteObject.exportObject(remobj,0);
		System.out.println("Exported");
		Naming.rebind("sum",remobj);
		System.out.println("Object Binded");
	}
}