package s;
import java.rmi.*;

public interface suminterf extends Remote{
	public int add(int a,int b) throws Exception;
	public int subtract(int a,int b) throws Exception;
	public int multiply(int a,int b) throws Exception;
	public int divide(int a,int b) throws Exception;
}