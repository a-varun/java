import java.rmi.*;
import s.*;

public class rmiclient{
	public static void main(String[] args) throws Exception{
		try{
			Remote ref = Naming.lookup("rmi://localhost:1099/sum");
			s.suminterf sum = (s.suminterf) ref;
			while(true){
				int a = 10;
				int b = 20;
				System.out.println("1.add");
				System.out.println("2.subtract");
				System.out.println("3.multiply");
				System.out.println("4.divide");
				System.out.println("5.quit");
				java.util.Scanner sc = new java.util.Scanner(System.in);
				int n= sc.nextInt();
				if(n==5) break;
				System.out.println("Enter the two ");
				a=sc.nextInt();
				b=sc.nextInt();
				switch(n){
					case 1:	System.out.println(sum.add(a,b));break;
					case 2:	System.out.println(sum.subtract(a,b));break;
					case 3:	System.out.println(sum.multiply(a,b));break;
					case 4:	System.out.println(sum.divide(a,b));break;
				}
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
	}
}