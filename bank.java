import java.io.*;
import java.util.*;

interface quotation
{
	void display();
}

interface payout
{
	int pays() throws Exception;
}

class invoice implements quotation,payout	+                                                                                                                      
{
	int n,cost[], numofitem[], totalcost[],given ;

	public void display()
	{
		for(int i=0;i<n;i++)
		{
		
			System.out.println("The cost of a single item is "+cost[i]);
			System.out.println("The number of items is "+numofitem[i]);
			System.out.println("The total cost is "+totalcost[i]);
		}
	}

	public void getdetail() throws Exception
	{
		System.out.println("Enter the number of items :");
		InputStreamReader ins=new InputStreamReader(System.in);
		BufferedReader br=new BufferedReader(ins);
		n=Integer.parseInt(br.readLine());
		cost=new int[n];
		numofitem=new int[n];
		totalcost=new int[n];
		for(int i=0;i<n;i++)
		{
			System.out.println("Enter the "+i+"th item detail.");
			cost[i]=Integer.parseInt(br.readLine());
			numofitem[i]=Integer.parseInt(br.readLine());
			totalcost[i]=cost[i]*numofitem[i];
		}

	}
	public int pays() throws Exception
	{
		System.out.println("Enter the amount given");
		int n11= Integer.parseInt((new BufferedReader(new InputStreamReader(System.in))).readLine());
		int c=0;
		for(int i=0;i<n;i++)
		{
			c+=totalcost[i];
		}
		return n11-c;
	}

}

public class bank
{
	public static void main(String[] args) throws Exception{
		invoice inv=new invoice();
		inv.getdetail();
		inv.display();
		System.out.println("The amount refunded: "+inv.pays());
	}
}