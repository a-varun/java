import java.io.*;

class enc
{
	public String Encrypt(String s)
	{
		String t1,t2;
		t1="";
		t2="";
		for(int i=0;i<s.length();i++)
		{
			char c=(char)((((int)(s.charAt(i))-(int)('a'>s.charAt(i)?'A':'a')+i)%26)+(int)('a'>s.charAt(i)?'A':'a'));
			if(i%2==0)
			{
				t2+=c;
			}
			else
			{
				t1=c+t1;
			}
		}
		return t1+t2;
	}
	public String Decrypt(String s)
	{
		String ans="";
		int n=0,ptr1,ptr2;
		ptr1=s.length()/2;
		ptr2=ptr1-1;
		while(true)
		{
			int c=(int)s.charAt(ptr1)-(int)('a'>s.charAt(n)?'A':'a');
			c-=n;
			while(c<0)
			{
				c+=26;
			}
			ans+=(char) (c+(int)('a'>s.charAt(n)?'A':'a'));
			ptr1++;
			n++;
			if(ptr1>=s.length()&&ptr2<0) return ans;
			c=(int)s.charAt(ptr2)-(int)('a'>s.charAt(n)?'A':'a');
			c-=n;
			while(c<0)
			{
				c+=26;
			}
			ans+=(char)(c+(int)('a'>s.charAt(n)?'A':'a'));
			ptr2--;
			n++;
			if(ptr1>=s.length()&&ptr2<0) return ans;
			
		}
	}
}

public class encrypt
{
	public static void main(String[] args) throws Exception{
		String a=new BufferedReader(new InputStreamReader(System.in)).readLine();
		enc nnn=new enc();
		System.out.print("Encrypted: ");
		System.out.println(nnn.Encrypt(a));
		System.out.print("Decrypted: ");
		System.out.println(nnn.Decrypt(nnn.Encrypt(a)));
	}
}