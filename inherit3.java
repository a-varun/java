import java.io.*;

class animal
{
	void print()
	{
		System.out.println("Animal");
	}
}

class mammal extends animal
{
	void print()
	{
		System.out.println("mammal");
	}
}

class reptile extends animal
{
	void print()
	{
		System.out.println("reptile");
	}
}

class dog extends mammal
{
	void print()
	{
		System.out.println("dog");
	}
}

public class inherit3
{
	public static void main(String[] args) {
		animal an[]=new animal[10];
		an[1]=new animal();
		an[1].print();

	}
}