import java.io.*;

class Student
{
	int marks;
	Student()
	{
		marks=10;
	}
	Student(int n)
	{
		marks=n;
	}
	final void display()
	{
		System.out.println(marks);
	}
}

class Boy extends Student
{
	Boy(int n)
	{
		marks=n;
	}
/*	void display()
	{
		System.out.println(marks*10);
	}
*/
}

class Girl extends Student
{
	int name=123;
	Girl(int n)
	{
		marks=n;
	}
/*	void display()
	{
		System.out.println(String.valueOf(name)+marks*1000);
	}
*/
}

public class final1
{
	public static void main(String[] args) {
	Student st= new Student(1000);
	st.display();
	st= new Girl(1000);
	st.display();

	}
}