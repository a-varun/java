import java.io.*;
import java.util.*;
class Solution {
    public int evalRPN(String[] token) {
        int l=token.length;
        Stack s=new Stack();
        for(int i=0;i<l;i++)
        {
            try
            {
                int n=Integer.parseInt(token[i]);
                s.push(new Integer(n));
            }
            catch(Exception e)
            {
                int res;
                if(token[i]=="+")
                {
                    res=Integer.parseInt(s.pop()) + Integer.parseInt(s.pop());
                }
                if(token[i]=="-")
                {
                    res=Integer.parseInt(s.pop()) - Integer.parseInt(s.pop());
                }
                if(token[i]=="*")
                {
                    res=Integer.parseInt(s.pop()) * Integer.parseInt(s.pop());
                }
                if(token[i]=="/")
                {
                    res=Integer.parseInt(s.pop()) / Integer.parseInt(s.pop());
                }
                s.push(res);
            }
        }
        return s.pop();
    }
}
public class a {
    public static void main(String[] args) {
        String s[] =  {"4", "13", "5", "/", "+"};
        Solution st = new Solution();
        System.out.println(st.evalRPN(s));
    }
}