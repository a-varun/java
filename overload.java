import java.util.*;

class mmul
{
	public int a[][],r,c;
	mmul(int r,int c)
	{
		a=new int[r][c];
		this.r=r;
		this.c=c;
	}

	void getElements()
	{
		java.util.Scanner s=new java.util.Scanner(System.in);
		System.out.println("Enter the matrix"); 
		for(int i=0;i<r;i++)
			for(int j=0;j<c;j++)
				a[i][j]=s.nextInt();
	}
	mmul multiply(mmul b)
	{
		if(this.c!=b.r)
		{
			return new mmul(0,0);
		}
		mmul c=new mmul(this.r,b.c);
		for(int i=0;i<this.r;i++)
		{
			for(int j=0;j<b.c;j++)
			{
				c.a[i][j]=0;
				for(int k=0;k<b.r;k++)
					c.a[i][j]+=this.a[i][k]*b.a[k][j];
			}
		}
		return c;
	}

	public void toString(mmul c)
	{
		for(int i=0;i<c.r;i++)
		{
			for(int j=0;j<c.c;j++)
				System.out.print(c.a[i][j]+" ");
			System.out.println();
		}
	}


}

class overload
{
	public static void main(String[] args) {
		mmul c,a,b;
		int r,cm;
		java.util.Scanner s=new java.util.Scanner(System.in);
		System.out.println("Enter the row and column of the first matrix:");
		r=s.nextInt();
		cm=s.nextInt();
		a=new mmul(r,cm);
		a.getElements();
		System.out.println("Enter the row and column of the second matrix:");
		r=s.nextInt();
		cm=s.nextInt();
		b=new mmul(r,cm);
		b.getElements();
		c=a.multiply(b);
		if(c.r==0&&c.c==0)
		{
			System.out.println("Matrix multiplication is not possible!");
		}
		else
		{
			c.toString(c);
		}
	}
}