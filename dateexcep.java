import java.io.*;

class dateexception extends Exception
{
	int n;
	dateexception(int k)
	{
		n=k;
	}
	void printexc()
	{
		if(n==1)
		System.out.println("No such date exists.");
		else if(n==2)
			System.out.println("No such month exists");
		else 
			System.out.println("Year out of bounds");
	}
}

public class dateexcep
{
	public static void main(String[] args) throws Exception
	{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		try{
			System.out.println("Enter the day");
			int day= Integer.parseInt(br.readLine());
			System.out.println("Enter the month");

			int month=Integer.parseInt(br.readLine());
			System.out.println("Enter the year");
			int year=Integer.parseInt(br.readLine());
			if(day>31||day<=0)
				throw new dateexception(1);
			if(month>12||month<=0)
				throw new dateexception(2);
			if(year<1000||year>2200)
				throw new dateexception(3);
			int ar[]={31,28,31,30,31,30,31,31,30,31,30,31};
			if((year%400==0)||(year%4==0&&year%100!=0))
				ar[1]=29;
			month-=1;

			if(day>ar[month])
				throw new dateexception(1);
			System.out.println("Date exists!");
		}
		catch(dateexception exp)
		{
			exp.printexc();
		}
	}
}