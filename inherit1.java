import java.io.*;

class Student
{
	int marks;
	Student()
	{
		marks=10;
	}
	Student(int n)
	{
		marks=n;
	}
	void display()
	{
		System.out.println(marks);
	}
}

class Boy extends Student
{
	Boy(int n)
	{
		marks=n;
	}
	void display()
	{
		System.out.println(marks*10);
	}
}

public class inherit1
{
	public static void main(String[] args) {
	Student st= new Student(1000);
	st.display();
	Boy by= new Boy(1000);
	by.display();

	}
}