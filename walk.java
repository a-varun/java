public class Main{
	public static void main(String[] args) {
		int ite;
		java.util.Scanner sc=new java.util.Scanner(System.in);
		ite=sc.nextInt();
		while(ite--){
			int k,n=sc.nextInt();
			int tp=0,mp=0;
			while(n--){
				k=sc.nextInt();
				if(tp>=k)tp--;
				else
				{
					mp+=k-tp;
					tp=k-1;
				}
			}
			System.out.println(mp);
		}
	}
}