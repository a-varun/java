class Link
{
	int data;
	Link next;
	Link(int a, Link n)
	{
		data=a;
		next=n;
	}
}

class Stack
{
	private Link a;
	Stack()
	{
		a=null;
	}

	void push(int n)
	{
		a=new Link(n,a);
	}
	int pop()
	{
		if(a==null) return -1;
		int p=a.data;
		a=a.next;
		return p;
	}
	int top()
	{
		if(a==null) return -1;
		return a.data;
	}
}

class linklist
{
	public static void main(String[] args) {
		Stack a;
		a=new Stack();
		String s;
		java.util.Scanner sc= new java.util.Scanner(System.in);
		for(;;)
		{
			s=sc.next();
			if(s.equals("top"))
			{
				System.out.println(a.top());
				continue;
			}
			if(s.equals("pop"))
			{
				System.out.println(a.pop());
				continue;
			}
			if(s.equals("push"))
			{
				int n=sc.nextInt();
				a.push(n);
			}
			else
				break;
		}
	}
}